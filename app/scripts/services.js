'use strict'

angular.module('sbAdminApp')
.constant("baseURL", "http//192.168.0.106:3000/")
.factory('testFactory', ['$resource', 'baseURL',function($resource, baseURL){
    
    return $resource(baseURL + "testCases", null, {
        'update': {
            method: 'PUT'
        }
    });
    
}]);